import time
from typing import Optional, Any, List
import enum
import base64
import hashlib
import logging
from os import path
from suds.client import Client, SoapClient, Method, Options
import pprp

logger = logging.getLogger(__name__)


class MTXPRBalanceDoors(enum.Enum):
    LEFT_OUTER = 'LeftOuter'
    RIGHT_OUTER = 'RightOuter'


class MTXPRBalance:
    SESSION_SERVICE = 'BasicHttpBinding_ISessionService'
    WEIGHING_SERVICE = 'BasicHttpBinding_IWeighingService'
    WEIGHING_TASK_SERVICE = 'BasicHttpBinding_IWeighingTaskService'
    DOSING_AUTOMATION_SERVICE = 'BasicHttpBinding_IDosingAutomationService'
    NOTIFICATION_SERVICE = 'BasicHttpBinding_INotificationService'
    DRAFT_SHIELDS_SERVICE = 'BasicHttpBinding_IDraftShieldsService'

    def __init__(self,
                 wsdl_path: str = './MT.Laboratory.Balance.XprXsr.V03.wsdl',
                 password: str = '123456',
                 connect: bool = True):
        self.logger = logger.getChild(self.__class__.__name__)
        self.wsdl_path = path.abspath(wsdl_path)
        self.password = password
        self.client: Optional[Client] = None
        self.session_id: Optional[str] = None

        if connect:
            self.connect()

    def request(self, service: str, method_name: str, args: List[Any] = [], include_session_id: bool = True, ignore_error: bool = False):
        method = getattr(self.client.service[service], method_name)
        method_args = args if not include_session_id else [self.session_id, *args]

        self.logger.debug(f'Sending request: {service}.{method_name}({", ".join([str(a) for a in args])})')
        response = method(*method_args)
        self.logger.debug(f'Response outcome: {response.Outcome}')

        if response.Outcome != 'Success' and not ignore_error:
            error_message = response.ErrorMessage if hasattr(response, 'ErrorMessage') else response.ErrorState
            self.logger.error(f'Error with request: {error_message}')
            raise MTXPRBalanceError(f'Error with request {service}.{method_name}({", ".join([str(a) for a in args])}): {response.ErrorMessage}')

        return response

    def connect(self):
        self.client = Client(f'file://localhost/{self.wsdl_path}')
        session_response = self.request(self.SESSION_SERVICE, 'OpenSession', include_session_id=False)
        self.session_id = self.decrypt_session_id(self.password, session_response.SessionId, session_response.Salt)

    def decrypt_session_id(self, password, encrypted_session_id, salt):
        decoded_session_id = base64.b64decode(encrypted_session_id)
        decoded_salt = base64.b64decode(salt)
        encoded_password = password.encode()
        key = hashlib.pbkdf2_hmac('sha1', encoded_password, decoded_salt, 1000, dklen=32)

        data_source = pprp.data_source_gen(decoded_session_id)
        decryption_gen = pprp.rijndael_decrypt_gen(key, data_source)
        session_id = pprp.decrypt_sink(decryption_gen)

        return session_id.decode()

    def zero(self):
        self.request(self.WEIGHING_SERVICE, 'Zero', ['false'])

    def weigh(self):
        response = self.request(self.WEIGHING_SERVICE, 'GetWeight')

        return response.WeightSample.NetWeight

    def set_door_position(self, door: MTXPRBalanceDoors, position: int):
        # We can create objects manually with a factory, or use the special dict syntax
        # shield_position = self.client.factory.create('DraftShieldPosition')
        # shield_position.DraftShieldId.value = door_name
        # shield_position.OpeningWidth = position
        # shield_position.OpeningSide = None

        # shield_positions = self.client.factory.create('ArrayOfDraftShieldPosition')
        # shield_positions.DraftShieldPosition.append(shield_position)

        # self.request(self.DRAFT_SHIELDS_SERVICE, 'SetPosition', [shield_positions])

        shield_position = {
            'DraftShieldId': door.value,
            'OpeningWidth': position,
            'OpeningSide': None
        }

        # we can use a dict to craete "arrayOf" types by using the name as the key with an array as the values
        # equivalent to:
        #         shield_positions = self.client.factory.create('ArrayOfDraftShieldPosition')
        #         shield_positions.DraftShieldPosition.append(shield_position)
        self.request(self.DRAFT_SHIELDS_SERVICE, 'SetPosition', [{'DraftShieldPosition': [shield_position]}])

    def open_door(self, door: MTXPRBalanceDoors):
        self.set_door_position(door, 100)

    def close_door(self, door: MTXPRBalanceDoors):
        self.set_door_position(door, 0)

    def get_door_position(self, door: MTXPRBalanceDoors) -> int:
        response = self.request(self.DRAFT_SHIELDS_SERVICE, 'GetPosition', [{
            'DraftShieldIdentifier': [door.value]
        }])
        door_position_info = response.DraftShieldsInformation.DraftShieldInformation[0]

        return door_position_info.OpeningWidth

    def is_door_open(self, door: MTXPRBalanceDoors) -> bool:
        position = self.get_door_position(door)

        return position > 0

    def find_automated_dosing_method(self):
        methods = self.request(self.WEIGHING_TASK_SERVICE, 'GetListOfMethods')

        for method_description in methods.Methods.MethodDescription:
            if method_description.MethodType == 'AutomatedDosing':
                return method_description

    def start_automated_dosing(self, substance_name: str, dose_amount_mg: float, lower_tolerance: float = 0.02, upper_tolerance: float = 0.02):
        dosing_method = self.find_automated_dosing_method()
        self.request(self.WEIGHING_TASK_SERVICE, 'StartTask', [dosing_method.Name])
        actual_dose_amount_mg = None

        dosing_job = {
            'SubstanceName': substance_name,
            'VialName': 'Vial',
            'TargetWeight': {'Value': dose_amount_mg, 'Unit': 'Milligram'},
            'LowerTolerance': {'Value': dose_amount_mg * lower_tolerance, 'Unit': 'Milligram'},
            'UpperTolerance': {'Value': dose_amount_mg * upper_tolerance, 'Unit': 'Milligram'},
        }

        self.request(self.DOSING_AUTOMATION_SERVICE, 'StartExecuteDosingJobListAsync', [{
            'DosingJob': [dosing_job]
        }])

        for poll_count in range(200):
            notifications_response = self.request(self.NOTIFICATION_SERVICE, 'GetNotifications', [500], ignore_error=True)
            for notification_type, notification in notifications_response.Notifications:
                if notification_type == 'DosingAutomationActionAsyncNotification':
                    action_type = notification.DosingJobActionType
                    if action_type in ('PlaceVial', 'RemoveVial', 'PlaceDosingHead', 'RemoveDosingHead'):
                        self.request(self.DOSING_AUTOMATION_SERVICE, 'ConfirmDosingJobAction', [action_type, notification.ActionItem])

                if notification_type == 'DosingAutomationJobFinishedAsyncNotification':
                    actual_dose_amount_mg = float(notification.DosingResult.WeightSample.NetWeight.Value)
                    self.logger.info(f'Dosing automation job finished, substance: {substance_name}, requested weight: {dose_amount_mg}, actual weight: {actual_dose_amount_mg}')

                if notification_type == 'DosingAutomationFinishedAsyncNotification':
                    if actual_dose_amount_mg is None:
                        raise MTXPRBalanceError(f'Dosing failed, substance: {substance_name}')

                    self.logger.info(f'Dosing job finished, substance: {substance_name}')
                    return actual_dose_amount_mg

            time.sleep(1)

        raise MTXPRBalanceError(f'Timeout while waiting for dosing job to finish, substance: {substance_name}')


class MTXPRBalanceError(Exception):
    pass
