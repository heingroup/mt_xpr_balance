import base64
import hashlib
import sys
from os import path
from suds.client import Client, SoapClient, Method, Options
import pprp
import logging
from mt_xpr_balance import MTXPRBalance, MTXPRBalanceDoors


logging.basicConfig(level=logging.INFO)
logging.getLogger('mt_xpr_balance').setLevel(logging.DEBUG)

balance = MTXPRBalance()
is_open = balance.is_door_open(MTXPRBalanceDoors.LEFT_OUTER)
print(is_open)
# dose_amount_mg = balance.start_automated_dosing('NACL', 50)
# print(f'Dosing finished, amount: {dose_amount_mg}')